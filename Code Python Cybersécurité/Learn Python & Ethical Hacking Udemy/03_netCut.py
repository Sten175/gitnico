#!/usr/bin/env python


# write this in shell to create a queue
# iptables -I FORWARD -j NFQUEUE --queue-num 0
# to delete the iptables -> iptables --flush
# apt-get install build-essential python-dev libnetfilter-queue-dev
# pip install netfilterqueue

import netfilterqueue


def process_packet(packet):
    print(packet)
    packet.drop()


queue = netfilterqueue.NetfilterQueue()
queue.bind(0, process_packet)
queue.run()


# iptables -I FORWARD -j NFQUEUE --queue-num 0
# iptables -I OUTPUT -j NFQUEUE --queue-num 0
# iptables -I INPUT -j NFQUEUE --queue-num 0